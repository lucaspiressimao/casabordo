package main

import (
	"database/sql"
	"net/http"

	"router"
	_ "github.com/lib/pq"
	"os"
)

func setupDB() *sql.DB {
	db, err := sql.Open("postgres", os.Getenv("DATABASE_URL"))
	if err != nil {
		panic(err)
	}
	err = db.Ping()
	if err != nil {
		panic(err)
	}
	// defer db.Close()
	return db
}

func main() {
	r := router.Router{DB: setupDB()}
	router := r.CreateRoutes()
	http.ListenAndServe(":2345", router)
}
