package models

import (
	"database/sql"
	"errors"
	"libs"
	_ "github.com/lib/pq"
	"fmt"
)

type Categoria struct {
	DB       *sql.DB `json:"-"`
	Id       string  `json:"id"`
	Nome     string  `json:"nome"`
}

func (c Categoria) Save() (Categoria, error) {

	v := libs.Validation{}

	if msg, err := v.MinSize(c.Nome, 4).Message(`{"error":"Nome muito pequeno"}`); err != true {
		return c, errors.New(msg)
	}

	if msg, err := v.MaxSize(c.Nome, 50).Message(`{"error":"Nome muito longo"}`); err != true {
		return c, errors.New(msg)
	}

	err := c.DB.QueryRow("INSERT INTO CategoriaProduto(nome) VALUES($1) RETURNING id", c.Nome).Scan(&c.Id)
	if err != nil {
		return c, err
	}
	return c, nil
}

func (c Categoria) ListarTodos() ([]Categoria, error) {
	results := make([]Categoria, 0)

	rows, err := c.DB.Query("SELECT id, nome FROM CategoriaProduto")

	if err != nil {
		panic(err)
	}
	defer rows.Close()
	for rows.Next() {
		err := rows.Scan(&c.Id, &c.Nome)
		if err != nil {
			panic(err)
		}
		results = append(results, c)
	}
	return results, nil
}

func (c Categoria) ListarCategoria() ([]Categoria, error) {
	result := make([]Categoria, 0)
	row, err := c.DB.Query("SELECT id, nome FROM CategoriaProduto WHERE id = $1", c.Id)

	if err != nil {
		panic(err)
	}
	defer row.Close()
	row.Next()
	fmt.Println(c)
	err2 := row.Scan(&c.Id, &c.Nome)
	if err2 != nil {
		panic(err2)
	}
	result = append(result, c)
	return result, nil
}
