package models

import (
	"database/sql"
	"errors"
	"libs"
	_ "github.com/lib/pq"
	"fmt"
)

type Cliente struct {
	DB       *sql.DB `json:"-"`
	Id       string  `json:"id"`
	Nome     string  `json:"nome"`
	Ddd	 string  `json:"ddd"`
	Telefone string  `json:"telefone"`
}

func (c Cliente) Save() (Cliente, error) {

	v := libs.Validation{}

	if msg, err := v.MinSize(c.Nome, 4).Message(`{"error":"Nome muito pequeno"}`); err != true {
		return c, errors.New(msg)
	}

	if msg, err := v.MaxSize(c.Nome, 50).Message(`{"error":"Nome muito longo"}`); err != true {
		return c, errors.New(msg)
	}

	if msg, err := v.ExactSize(c.Ddd, 3).Message(`{"error":"Ddd deve ser informado com 3 numero"}`); err != true {
		return c, errors.New(msg)
	}

	if msg, err := v.MinSize(c.Telefone, 8).Message(`{"error":"Telefone deve conter ao menos 8 caracteres"}`); err != true {
		return c, errors.New(msg)
	}

	err := c.DB.QueryRow("INSERT INTO Cliente(nome, ddd, telefone) VALUES($1, $2, $3) RETURNING id", c.Nome, c.Ddd, c.Telefone).Scan(&c.Id)
	if err != nil {
		return c, err
	}
	return c, nil
}

func (c Cliente) ListarTodos() ([]Cliente, error) {
	results := make([]Cliente, 0)

	rows, err := c.DB.Query("SELECT id, nome, ddd, telefone  FROM Cliente")

	if err != nil {
		panic(err)
	}
	defer rows.Close()
	for rows.Next() {
		err := rows.Scan(&c.Id, &c.Nome, &c.Ddd, &c.Telefone)
		if err != nil {
			panic(err)
		}
		results = append(results, c)
	}
	return results, nil
}

func (c Cliente) ListarCliente() ([]Cliente, error) {
	result := make([]Cliente, 0)
	row, err := c.DB.Query("SELECT id, nome, ddd, telefone FROM Cliente WHERE id = $1", c.Id)

	if err != nil {
		panic(err)
	}
	defer row.Close()
	row.Next()
	err2 := row.Scan(&c.Id, &c.Nome, &c.Ddd, &c.Telefone)
	fmt.Println(c)
	if err2 != nil {
		panic(err2)
	}
	result = append(result, c)
	return result, nil
}
