package models

import (
	"database/sql"
	"errors"
	"libs"
	_ "github.com/lib/pq"
)

type Produto struct {
	DB       		*sql.DB `json:"-"`
	Id       		string  `json:"id"`
	Nome     		string  `json:"nome"`
	IdCategoria		string  `json:"id_catagoria"`
	TipoValidade 		string  `json:"tipo_validade"`
	Validade 		string  `json:"validade"`
	Estoque 		string  `json:"estoque"`
	EstoqueMin 		string  `json:"estoque_min"`
}

func (p Produto) Save() (Produto , error) {

	v := libs.Validation{}

	if msg, err := v.MinSize(p.Nome, 4).Message(`{"error":"Nome muito pequeno"}`); err != true {
		return p, errors.New(msg)
	}

	if msg, err := v.MaxSize(p.Nome, 50).Message(`{"error":"Nome muito longo"}`); err != true {
		return p, errors.New(msg)
	}

	err := p.DB.QueryRow("INSERT INTO Produto (nome, id_categoria, periodo_validade, validade, estoque, estoque_min) VALUES($1, $2, $3, $4, $5, $6) RETURNING id", p.Nome, p.IdCategoria, p.TipoValidade, p.Validade, p.Estoque, p.EstoqueMin).Scan(&p.Id)
	if err != nil {
		return p, err
	}
	return p, nil
}

func (p Produto) AtualizaEstoque(quantidade int) ([]Produto , error) {
	results := make([]Produto , 0)

	_, err := p.DB.Query("UPDATE Produto SET estoque = $1 WHERE id = $2 ", quantidade, p.Id)
	rows, err := p.DB.Query("SELECT id, nome, id_categoria, periodo_validade, validade, estoque, estoque_min FROM Produto")

	if err != nil {
		panic(err)
	}
	defer rows.Close()
	for rows.Next() {
		err := rows.Scan(&p.Id, &p.Nome, &p.IdCategoria, &p.TipoValidade, &p.Validade, &p.Estoque, &p.EstoqueMin)
		if err != nil {
			panic(err)
		}
		results = append(results, p)
	}
	return results, nil
}

func (p Produto) ListarTodos() ([]Produto , error) {
	results := make([]Produto , 0)

	rows, err := p.DB.Query("SELECT id, nome, id_categoria, periodo_validade, validade, estoque, estoque_min FROM Produto ")

	if err != nil {
		panic(err)
	}
	defer rows.Close()
	for rows.Next() {
		err := rows.Scan(&p.Id, &p.Nome, &p.IdCategoria, &p.TipoValidade, &p.Validade, &p.Estoque, &p.EstoqueMin)
		if err != nil {
			panic(err)
		}
		results = append(results, p)
	}
	return results, nil
}

func (p Produto) ListarProduto() ([]Produto , error) {
	result := make([]Produto , 0)
	row, err := p.DB.Query("SELECT id, nome, id_categoria, periodo_validade, validade, estoque, estoque_min FROM Produto WHERE id = $1", p.Id)

	if err != nil {
		panic(err)
	}
	defer row.Close()
	row.Next()
	err2 := row.Scan(&p.Id, &p.Nome, &p.IdCategoria, &p.TipoValidade, &p.Validade, &p.Estoque, &p.EstoqueMin)

	if err2 != nil {
		panic(err2)
	}
	result = append(result, p)
	return result, nil
}
