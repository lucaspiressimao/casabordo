package router

import (
	"database/sql"
	"net/http"
	"fmt"
	"github.com/gorilla/mux"
)

type Handlers func(w http.ResponseWriter, req *http.Request)

type Router struct {
	DB *sql.DB
}

func (r Router) CreateRoutes() *mux.Router {

	router := mux.NewRouter().StrictSlash(true)
	fmt.Printf("Criando rotas \n")
	m := map[string]map[string]Handlers{
		"POST": {
			"/cliente":       	 	r.criarCliente,
			"/produto/categoria":	 	r.cadastrarCategoria,
			"/produto":		 	r.cadastrarProduto,
		},
		"GET": {
		 	"/clientes":		 	r.listarClientes,
			"/cliente/{id}":	 	r.listarCliente,
			"/produto/categorias":	 	r.listarCategorias,
			"/produto/categoria/{id}":	r.listarCategoria,
			"/produtos":			r.listarProdutos,
			"/produto/{id}":		r.listarProduto,
		},
//		"PUT": {
//			"/produto/{id}/estoque":	r.atualizaEstoque,
//		}
	}

	for method, routes := range m {
		for route, function := range routes {
			router.HandleFunc(route, function).Methods(method)
		}
	}
	return router
}
