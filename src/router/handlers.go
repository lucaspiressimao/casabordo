package router

import (
	"encoding/json"
	"net/http"
	"io"
	"models"
	"github.com/gorilla/mux"
	"fmt"
)

func (r Router) criarCliente(w http.ResponseWriter, req *http.Request) {
	nome := req.FormValue("nome")
	ddd := req.FormValue("ddd")
	telefone := req.FormValue("telefone")

	cliente := models.Cliente{Nome: nome, Ddd: ddd, Telefone: telefone, DB: r.DB}
	result, err := cliente.Save()

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	if err != nil {
		w.WriteHeader(500)
		io.WriteString(w, err.Error())
	} else {
		w.WriteHeader(201)
		json.NewEncoder(w).Encode(result)
	}
}

func (r Router) listarClientes(w http.ResponseWriter, req *http.Request) {
	clientes := models.Cliente{DB: r.DB}
	result, err := clientes.ListarTodos()

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	if err != nil {
		w.WriteHeader(500)
		io.WriteString(w, err.Error())
	} else {
		w.WriteHeader(200)
		json.NewEncoder(w).Encode(result)
	}
}

func (r Router) listarCliente(w http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	id := vars["id"]

	fmt.Println("Id requisitado %d", id)

	cliente := models.Cliente{Id: id, DB: r.DB}
	result, err := cliente.ListarCliente()

	fmt.Println(result)

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	if err != nil {
		w.WriteHeader(500)
		io.WriteString(w, err.Error())
	} else {
		w.WriteHeader(200)
		json.NewEncoder(w).Encode(result)
	}
}


func (r Router) cadastrarCategoria(w http.ResponseWriter, req *http.Request) {
	nome := req.FormValue("nome")

	categoria := models.Categoria{Nome: nome, DB: r.DB}
	result, err := categoria.Save()

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	if err != nil {
		w.WriteHeader(500)
		io.WriteString(w, err.Error())
	} else {
		w.WriteHeader(201)
		json.NewEncoder(w).Encode(result)
	}
}

func (r Router) listarCategorias(w http.ResponseWriter, req *http.Request) {
	categorias := models.Categoria{DB: r.DB}
	result, err := categorias.ListarTodos()

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	if err != nil {
		w.WriteHeader(500)
		io.WriteString(w, err.Error())
	} else {
		w.WriteHeader(200)
		json.NewEncoder(w).Encode(result)
	}

}

func (r Router) listarCategoria(w http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	id := vars["id"]

	categoria := models.Categoria{Id: id, DB: r.DB}
	result, err := categoria.ListarCategoria()

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	if err != nil {
		w.WriteHeader(500)
		io.WriteString(w, err.Error())
	} else {
		w.WriteHeader(200)
		json.NewEncoder(w).Encode(result)
	}
}





func (r Router) listarProdutos(w http.ResponseWriter, req *http.Request) {
	produtos := models.Produto{DB: r.DB}
	result, err := produtos.ListarTodos()

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	if err != nil {
		w.WriteHeader(500)
		io.WriteString(w, err.Error())
	} else {
		w.WriteHeader(200)
		json.NewEncoder(w).Encode(result)
	}

}

func (r Router) listarProduto(w http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	id := vars["id"]

	produto := models.Produto{Id: id, DB: r.DB}
	result, err := produto.ListarProduto()

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	if err != nil {
		w.WriteHeader(500)
		io.WriteString(w, err.Error())
	} else {
		w.WriteHeader(200)
		json.NewEncoder(w).Encode(result)
	}
}

func (r Router) cadastrarProduto(w http.ResponseWriter, req *http.Request) {
	nome := req.FormValue("nome")
	nome := req.FormValue("id_categoria")
	nome := req.FormValue("periodo_validade")
	nome := req.FormValue("validade")
	nome := req.FormValue("estoque")
	nome := req.FormValue("estoque_min")

	categoria := models.Categoria{Nome: nome, DB: r.DB}
	result, err := categoria.Save()

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	if err != nil {
		w.WriteHeader(500)
		io.WriteString(w, err.Error())
	} else {
		w.WriteHeader(201)
		json.NewEncoder(w).Encode(result)
	}
}
