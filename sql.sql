BEGIN TRANSACTION;

CREATE TABLE Cliente(
	id SERIAL PRIMARY KEY,
    nome VARCHAR,
    ddd SMALLINT,
    telefone INTEGER
	);


CREATE TABLE CategoriaProduto (
    id SERIAL PRIMARY KEY,
    nome VARCHAR
    );
        
CREATE TABLE Produto (
    id SERIAL PRIMARY KEY,
    nome VARCHAR,
    id_categoria INTEGER REFERENCES CategoriaProduto(id),
    periodo_validade CHAR(1),
    validade SMALLINT,
    estoque SMALLINT,
    estoque_min SMALLINT
    );

CREATE TABLE Preco (
    id SERIAL PRIMARY KEY,
    id_produto INTEGER REFERENCES Produto(id),
    valor NUMERIC(14,2),
    data_inicio DATE
    );

CREATE TABLE Pedido (
    id SERIAL PRIMARY KEY,
    id_cliente INTEGER REFERENCES Cliente(id),
    data TIMESTAMP,
    total_pago NUMERIC(14,2)
	);

CREATE TABLE PedidoItem (
    id SERIAL PRIMARY KEY,
    id_pedido INTEGER REFERENCES Pedido(id),
    id_produto INTEGER REFERENCES Produto(id),
    quantidade SMALLINT,
    desconto NUMERIC(14,2),
    entregue BOOLEAN,
    pago BOOLEAN
	);

COMMIT;
-- ROLLBACK;
